Promise = require('bluebird');
var gulp = require('gulp');
var sass = require('gulp-sass');
var runSequence = require('run-sequence');
var rimraf = require('gulp-rimraf');
var merge = require('merge-stream');
var copy = require('gulp-copy');
var del = require('del');

var rm = require('gulp-rm');
var rename = require('gulp-rename');
var angularTemplatecache = require('gulp-angular-templatecache');
var usemin = require('gulp-usemin');
var minifyCss = require('gulp-minify-css');
var ngAnnotate = require('gulp-ng-annotate');
var uglify = require('gulp-uglify');
var rev = require('gulp-rev');


var postcss = require('gulp-postcss');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('autoprefixer');

var config = {
    bootstrapDir: './assets/bower/bootstrap-sass',
    faDir: './assets/bower/font-awesome',
    publicDir: './dist',
};

var deployDir = config.publicDir;

gulp.task('clean', function(cb) {
    return gulp.src([deployDir + '/*', '!' + deployDir + '/.hg'], {
        read: false
    }).pipe(rimraf({
        force: true
    }));
});

gulp.task('copy', function() {
    return gulp.src(['assets/img/*', 'assets/img/**/*', 'assets/fonts/*', 'assets/content/*', 'app/**/*', 'app/*', 'Caddyfile'])
        .pipe(copy(deployDir + '/'));
});


var compileScss = function() {
    return gulp.src('./assets/css/scss/app.scss')
        .pipe(sass({
            includePaths: ['./assets/css', config.bootstrapDir + '/assets/stylesheets', config.faDir + '/scss'],
        }))
        // .pipe(minifyCss())
        .pipe(gulp.dest('./assets/css'));
}

gulp.task('css', compileScss);


gulp.task('bootstrap-fonts', function() {
    return gulp.src(config.bootstrapDir + '/assets/fonts/**/*')
        .pipe(gulp.dest(config.publicDir + '/assets/fonts'));
});

gulp.task('fontawesome', function() {
    return gulp.src('assets/bower/font-awesome/fonts/*')
        .pipe(gulp.dest(deployDir + '/assets/fonts'));
});

gulp.task('fonts', ['bootstrap-fonts', 'fontawesome'])

gulp.task('templates', function() {
    return gulp.src('app/**/*.html')
        .pipe(angularTemplatecache({
            module: "app",
            root: '/app/'
        }))
        .pipe(gulp.dest('app'));
});

gulp.task('minify', function() {
    return gulp.src('index.html')
        .pipe(usemin({
            css: [sourcemaps.init(), postcss([autoprefixer({
                browsers: ['last 2 versions']
            })]), minifyCss(), 'concat', sourcemaps.write('.')],
            js: [ngAnnotate(), uglify(), 'concat']
        }))
        .pipe(rev())
        .pipe(gulp.dest(deployDir + '/'));
});

gulp.task('rename:index', function() {
    return gulp.src(deployDir + '/index-*.html')
        .pipe(rename('index.html'))
        .pipe(gulp.dest(deployDir + "/"));
});

gulp.task('remove:index', function() {
    return gulp.src(deployDir + '/index-*.html').pipe(rm())
});

gulp.task('watch', ['default'], function() {
    var watchFiles = [
        'app/**/*.html',
        'index.html'
    ];
    gulp.watch(watchFiles, ['templates']);
    gulp.watch(['assets/css/*', 'assets/css/scss/*'], ['css']);
});

gulp.task('default', function(done) {
    runSequence(
        'clean',
        'css',
        'copy',
        'fonts',
        'templates',
        'minify',
        'rename:index',
        'remove:index',
        function(error) {
            if (error) {
                console.log(error.message);
            } else {
                console.log('RELEASE FINISHED SUCCESSFULLY');
            }
            done(error);
        });
});
