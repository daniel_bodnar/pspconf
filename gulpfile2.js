Promise = require('bluebird');
var gulp = require('gulp');
var runSequence = require('run-sequence');
var rimraf = require('gulp-rimraf');
var merge = require('merge-stream');
var del = require('del');
$ = require('gulp-load-plugins')(); // Note the extra parens

var deployDir = 'dist';

gulp.task('clean', function(cb) {
    return gulp.src([deployDir + '/*', '!'+deployDir+'/.hg'], {
        read: false
    }).pipe(rimraf({
        force: true
    }));
});

gulp.task('copy', function() {
    return gulp.src(['assets/img/*', 'assets/img/**/*','assets/fonts/*', 'assets/content/*', 'app/**/*', 'app/*', 'Caddyfile'])
        .pipe($.copy(deployDir + '/'));
});

gulp.task('bootstrap-fonts', function() {
    return gulp.src('assets/bower/bootstrap/fonts/*')
        .pipe(gulp.dest(deployDir + '/assets/fonts'));
});

gulp.task('fontawesome', function() {
    return gulp.src('assets/bower/font-awesome/fonts/*')
        .pipe(gulp.dest(deployDir + '/assets/fonts'));
});

gulp.task('fonts', ['bootstrap-fonts', 'fontawesome'])

gulp.task('templates', function() {
    return gulp.src('app/**/*.html')
        .pipe($.angularTemplatecache({
            module: "app",
            root: '/app/'
        }))
        .pipe(gulp.dest('app'));
});

gulp.task('minify', function() {
    return gulp.src('index.html')
        .pipe($.usemin({
            css: [$.minifyCss(), 'concat'],
            js: [$.ngAnnotate(), $.uglify(), 'concat']
        }))
        .pipe($.rev())
        .pipe(gulp.dest(deployDir + '/'));
});

gulp.task('rename:index', function() {
    return gulp.src(deployDir + '/index-*.html')
        .pipe($.rename('index.html'))
        .pipe(gulp.dest(deployDir + "/"));
});

gulp.task('remove:index', function() {
    return gulp.src(deployDir + '/index-*.html').pipe($.rm())
});

gulp.task('watch', function() {
    var watchFiles = [
        'app/**/*.html'
    ];

    gulp.watch(watchFiles, ['templates']);
});

gulp.task('default', function(done){
  runSequence(
      'clean',
      'copy',
      'fonts',
      'templates',
      'minify',
      'rename:index',
      'remove:index',
      function (error) {
        if (error) {
          console.log(error.message);
        } else {
          console.log('RELEASE FINISHED SUCCESSFULLY');
        }
        done(error);
      });
});
