angular.module('app')

.directive('init', function($rootScope, $location, $window, $timeout, anchorSmoothScroll) {
  return {
    restrict: 'A',
    link: function($scope, element, attrs) {
      console.log('initializing...')
      init();

      var id = $location.hash();
      var el = angular.element('#' + id);
      $timeout(function() {
        if (el[0] && el[0].offsetTop) {
          var height = angular.element('#headerMenu').height();
          var scroll = angular.element(window).scrollTop();

          if (scroll > height) {
            angular.element(".header-hide").addClass("scroll-header");
          }

          anchorSmoothScroll.scrollTo(id);
        }
      }, 500);
    }
  }
})
.filter('stripbr', function() {
  return function(input) {
    return (input || '').replace('<br>', '');
  };
})
// .directive('growShadow', function($interval, $timeout) {
//     return {
//         restrict: 'A',
//         link: function($scope, element, attrs) {
//             var t = function(){
//                 element.addClass('shadow');
//             }

//             $timeout(t, 100);

//         }
//     }
// })

.directive('autoheight', function() {
  return {
    restrict: 'C',
    link: function($scope, element, attrs) {
      var setElementHeight = function() {
        var height = angular.element(window).height();
        element.css('min-height', (height));
      }
      angular.element(window).on("resize", function() {
        setElementHeight();
      }).resize();

      setElementHeight();
    }
  }
})

.directive('iframeRatio', function() {
  return {
    restrict: 'A',
    link: function($scope, element, attrs) {
      var setElementHeight = function() {
        var width = element.width();
        var height = width / parseFloat(attrs.iframeRatio);
        element.css('min-height', (height));
      }
      angular.element(window).on("resize", function() {
        setElementHeight();
      }).resize();
      setElementHeight();
    }
  }
})

.directive('jssor', function() {
  return {
    restrict: 'A',
    link: function($scope, element, attrs) {
      var bodyWidth = document.body.clientWidth;
      if (bodyWidth)
        element.$SetScaleWidth(Math.min(bodyWidth, 980));
      else
        window.setTimeout(ScaleSlider, 30);


      if (!navigator.userAgent.match(/(iPhone|iPod|iPad|BlackBerry|IEMobile)/)) {
        $(window).bind('resize', ScaleSlider);
      }
    }
  }
})

// .directive('smoothScroll', function() {
//     return {
//         restrict: 'A',
//         link: function($scope, element, attrs) {

//             var height = element.height();
//             var scroll = angular.element(window).scrollTop();
//             if (scroll > height) {
//                 angular.element(".header-hide").addClass("scroll-header");
//             }

//             smoothScroll.init({
//                 speed: 1000,
//                 easing: 'easeInOutCubic',
//                 offset: height,
//                 updateURL: false
//             });

//             angular.element(window).scroll(function() {
//                 var height = angular.element(window).height();
//                 var scroll = angular.element(window).scrollTop();
//                 if (scroll) {
//                     angular.element(".header-hide").addClass("scroll-header");
//                 } else {
//                     angular.element(".header-hide").removeClass("scroll-header");
//                 }

//             });
//         }
//     }
// })

.directive('scrollTo', function($location, anchorSmoothScroll) {
  return {
    restrict: 'A',
    link: function($scope, element, attrs) {
      var id = attrs.scrollTo;
      element.click(function(event) {
        $location.hash(id);
        $scope.$apply(function(){
          anchorSmoothScroll.scrollTo($location.hash());
        });
      });
    }
  }
})



.directive('venobox', function() {
  return {
    restrict: 'C',
    link: function($scope, element, attrs) {
      element.venobox({
        numeratio: true,
        infinigall: true,
        border: '20px'
      })
    }
  }
})

.directive('venoboxvid', function() {
  return {
    restrict: 'C',
    link: function($scope, element, attrs) {
      element.venobox({
        bgcolor: '#000'
      })
    }
  }
})


.directive('venoboxframe', function() {
  return {
    restrict: 'C',
    link: function($scope, element, attrs) {
      element.venobox({
        border: '6px'
      })
    }
  }
})

.directive('venoboxinline', function() {
  return {
    restrict: 'C',
    link: function($scope, element, attrs) {
      element.venobox({
        framewidth: '300px',
        frameheight: '250px',
        border: '6px',
        bgcolor: '#f46f00'
      })
    }
  }
})

.directive('venoboxajax', function() {
  return {
    restrict: 'C',
    link: function($scope, element, attrs) {
      element.venobox({
        border: '30px;',
        frameheight: '220px'
      })
    }
  }
})

.directive('gridGallery', function() {
  return {
    restrict: 'C',
    link: function($scope, element, attrs) {
      new CBPGridGallery(element[0]);
    }
  }
})

.directive('tabsUi', function() {
  return {
    restrict: 'C',
    link: function($scope, element, attrs) {
      new CBPFWTabs(element[0]);
    }
  }
})

// .filter('truncate', function ($filter) {
//   return function (text, length, end) {
//     if (isNaN(length)) {
//       length = 10;
//     }

//     if (end === undefined) {
//       end = '...';
//     }

//     if (text.length <= length || text.length - end.length <= length) {
//       return text;
//     } else {
//       return String(text).substring(0, length-end.length) + end;
//     }
//   };
// })

.directive('readMore', function($filter) {
  return {
    restrict: 'A',
    scope: {
      text: '=readMore',
      labelExpand: '@readMoreLabelExpand',
      labelCollapse: '@readMoreLabelCollapse',
      limit: '@readMoreLimit'
    },
    transclude: true,
    template: '<span ng-transclude ng-bind-html="text"></span> <a href="javascript:;" ng-click="toggleReadMore()" ng-bind="label"></a>',
    link: function(scope /*, element, attrs */ ) {

      var originalText = scope.text;

      scope.label = scope.labelExpand;

      scope.$watch('expanded', function(expandedNew) {
        if (expandedNew) {
          scope.text = originalText;
          scope.label = scope.labelCollapse;
        } else {
          scope.text = $filter('words')(originalText, scope.limit);
          scope.label = scope.labelExpand;
        }
      });

      scope.toggleReadMore = function() {
        scope.expanded = !scope.expanded;
      };

    }
  };
})

.directive('openModal', function($uibModal, $timeout) {
  return {
    restrict: 'A',
    link: function($scope, element, attrs) {
      element.css('cursor', 'pointer');
      element.click(function() {
        var modal = $uibModal.open({
          windowTemplateUrl: '/app/shared/modals/container.html',
          templateUrl: '/app/shared/modals/' + attrs['openModal'] + '.html',
          backdrop: true,
          animation: false,

          resolve: {
            name: function() {
              return attrs['openModal'];
            }
          },
          controller: function($scope, $uibModalInstance, name) {
            $scope.template = name || 'default';
            $scope.modalInstance = $uibModalInstance;

            $scope.$on('modal-closed', function() {
              $timeout(function() {
                $uibModalInstance.dismiss();
              }, 300);
            });
          }
        });

      });
    }
  }
})

.directive('mdModal', function($uibModal, $timeout) {
  return {
    restrict: 'C',
    link: function($scope, element, attrs) {

      var el = element;
      var overlay = angular.element('.modal-backdrop');
      $scope.close = removeModal;
      var close = el.find('.md-close').click(removeModal);
      overlay.click(removeModal);

      function removeModal() {
        el.removeClass('md-show');
        angular.element(document).removeClass('md-perspective');
        $scope.$emit('modal-closed')
      }

      $timeout(function() {
        el.addClass('md-show');
      }, 50);
    }
  }
})
