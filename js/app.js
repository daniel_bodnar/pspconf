angular.module('app', [
  'ui.router',
  // 'ngResource',
  'ngSanitize',
  'truncate',
  //'lbServices',
  //'truncate',
  'ngCookies',
  'angular-nicescroll',
  'ui.bootstrap'
])

.config(['$urlRouterProvider', '$locationProvider', '$httpProvider', '$stateProvider',
    function($urlRouterProvider, $locationProvider, $httpProvider, $stateProvider) {

      delete $httpProvider.defaults.headers.common['X-Requested-With'];
      $stateProvider
        .state('home', {
          url: '/',
          controller: 'homeCtrl',
          controllerAs: 'home',
          templateUrl: '/app/home/index.html'
        });

      $locationProvider.html5Mode(true);
      $locationProvider.hashPrefix('#');
    }
  ])
  .config(['$compileProvider', function($compileProvider) {
    $compileProvider.debugInfoEnabled(false);
  }])
  .controller('homeCtrl', function($scope, $rootScope, $state, $uibModal, $timeout) {
    $scope.selectTab = function(){
      return false;
    }
    $scope.speakers = [{
      id: 'jamesmason',
      name: 'James <br>Mason',
      title: 'Perspectives, National Director',
      sessions: [
        {
          title: 'The Awakened Church',
          description: 'Casting our minds and hearts toward the powerful <br>vision and specific mission of Perspectives.'
        },
        {
          title: 'Our future, Our family',
          description: 'Identifying and dreaming about the things <br>God will do through a team united in work and mission.'
        }
      ]
    },{
      id: 'yvonne',
      name: 'Yvonne <br>Huneycutt',
      title: 'Perspectives',
      sessions: [
        {
          title: 'Identifying with the Movement',
          description: 'Tracing and celebrating the Frontier mission movement and exploring Perspectives\' unique role and impact along with what must still be accomplished.'
        }
      ]
    },{
      id: 'johnlo',
      name: 'John <br>Lo',
      title: 'Epicentre Church, Lead Pastor',
      sessions: [
        {
          title: 'The Awakened Church',
          description: 'Looking further into the body of Christ what has been awakened <br>to the fulfillment of God\'s purposes among the nations of the earth.'
        }
      ]
    },{
      id: 'maryho',
      name: 'Mary <br>Ho',
      title: 'All Nations Family, Executive Director',
      sessions: [
        {
          title: 'The Values that Guide Us',
          description: 'Pressing into critical values and cultural habits that must prevail so that the Perspectives vision remains vibrant.'
        }
      ]
    },{
      id: 'drewjackson',
      name: 'Drew <br>Jackson',
      title: 'GraceWay Community Church, Pastor of Community Engagement',
      sessions: [
        {
          title: 'The Values that Guide Us',
          description: 'Pressing into critical values and cultural habits that must prevail so that the Perspectives vision remains vibrant.'
        }
      ]
    },{
      id: 'stevehawthorne',
      name: 'Steve <br>Hawthorne',
      title: 'WayMakers, Director',
      sessions: [
        {
          title: 'Our Unique Calling',
          description: 'Exploring mobilization as a profound calling and privilege as God activates <br>the Body of Christ toward the fulfillment of his purposes.'
        }
      ]
    },{
      id: 'perrin',
      name: 'Bishop <br>Perrin',
      title: 'Great Commission Global Ministries, President',
      sessions: [
        {
          title: 'Biblical Devotion',
          description: 'Exposition of scripture on the body of Christ as we elevate Perspectives before God and look forward to what He has in store for us.'
        }
      ]
    }]
  })

.run(function($rootScope, $state, $location, $window, Settings, anchorSmoothScroll, $timeout) {
  $rootScope.settings = Settings;
  $timeout(function() {
    $rootScope.player = new YT.Player('intro-video', {
      height: '100%',
      width: '100%',
      playerVars: {
        autoplay: 0,
        autohide: 1,
        showinfo: 0,
        controls: 1,
        rel: 0,
        disablekb: 0,
        modestbranding: 1,
        playsinline: 0
      },
      videoId: 'H4wPDBpSjpM',
      events: {
        onStateChange: $rootScope.video.onStateChange
      }
    });
  }, 1000);

  var video = $rootScope.video = {
    show: false,
    state: -1,
    close: function(){
      $rootScope.player.stopVideo();
      $rootScope.video.show = false;
    },
    play: function() {
      $rootScope.video.show = true;
      $rootScope.player.playVideo();
    },
    onStateChange: function(event) {
      $rootScope.video.state = event.data;
      if (event.data === 0) {
        $rootScope.video.show = false;
      }
      video.state = event.data;
      $rootScope.$apply();
    }
  }
  angular.element($window).on('keydown', function(e) {
      console.log(e);
      if(e.which === 27 && video.show){
        video.close();
        e.preventDefault();
      }
  });

  var wow = new WOW({
    boxClass: 'wow', // animated element css class (default is wow)
    animateClass: 'animated', // animation css class (default is animated)
    offset: 0, // distance to the element when triggering the animation (default is 0)
    mobile: false, // trigger animations on mobile devices (default is true)
    live: false // act on asynchronously loaded content (default is true)
  });

  wow.init();

  $rootScope.$on('$routeChangeStart', function(next, current) {
    //when the view changes sync wow
    wow.sync();
  });

  angular.element($window).bind("scroll", function() {
    var scroll = angular.element(window).scrollTop();
    if (angular.element(window).scrollTop()) {
      angular.element(".header-hide").addClass("scroll-header");
    } else {
      angular.element(".header-hide").removeClass("scroll-header");
    }
  });

  console.log('location hash:', $location.hash());




  // $rootScope.$on('$routeChangeSuccess', function(newRoute, oldRoute) {
  //   if ($location.hash()) $anchorScroll();
  // });

  // $rootScope.goto = function(elem) {
  //   // set the location.hash to the id of
  //   // the element you wish to scroll to.
  //   $location.hash(elem);

  //   // call $anchorScroll()
  //   anchorSmoothScroll.scrollTo(elem);
  // };
})
